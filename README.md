This image starts a nginx on `10.0.2.111` and starts tcpdump to show
incoming/outgoing Ethernet packets.

`start.sh` redirects `127.0.0.1:8090` on the host to nginx inside the guest:
<http://127.0.0.1:8090/>.
